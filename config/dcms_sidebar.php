<?php

return [
    "Faq" => [
        "icon"  => "fa-question",
        "links" => [
            ["route" => "admin/faq", "label" => "Faq", "permission" => 'faq-browse'],
            ["route" => "admin/faq/categories", "label" => "Categories", "permission" => "faq-browse"],
        ],
    ],
];
