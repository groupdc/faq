<?php

namespace Dcms\Faq\Models;

use Dcms\Core\Models\EloquentDefaults;

class Faq extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table = 'faq';

    public function detail()
    {
        return $this->hasMany('Dcms\Faq\Models\Detail', 'faq_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo("Dcms\Faq\Models\Category", 'faq_category_id', 'id');
        //return $this->hasOne("Category");
    }

    public function productinformation()
    {
        return $this->belongsToMany('Dcms\Faq\Models\Information', 'faq_to_products_information_group', 'faq_id', 'information_id')->withTimestamps();
    }

    public function plants()
    {
        return $this->belongsToMany('Dcms\Plants\Models\Plant', 'faq_to_plant', 'faq_id', 'plant_id')->withTimestamps();
    }

    public function articles()
    {
        return $this->belongsToMany('Dcms\Dcmsarticles\Models\Article', 'article_to_faq', 'faq_id', 'article_id')->withTimestamps();
    }
}
