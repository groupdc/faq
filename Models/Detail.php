<?php

namespace Dcms\Faq\Models;

use Dcms\Core\Models\EloquentDefaults;

class Detail extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "faq_language";
    protected $fillable = array('language_id', 'faq_id', 'title', 'text', 'description', 'script');

    public function faq()
    {
        return $this->belongsTo('Dcms\Faq\Models\Faq', 'faq_id', 'id');
    }

    public function faqcategory()
    {
        return $this->belongsTo('Dcms\Faq\Models\Category', 'faq_category_id', 'id');
    }

    public function pages()
    {
        // BelongsToMany belongsToMany(string $related, string $table = null, string $foreignKey = null, string $otherKey = null, string $relation = null)
        return $this->belongsToMany('Dcms\Pages\Models\Pageslanguage', 'faq_language_to_pages', 'faq_detail_id', 'page_id');
    }

    //		public function products()
//		{
//			// BelongsToMany belongsToMany(string $related, string $table = null, string $foreignKey = null, string $otherKey = null, string $relation = null)
//			return $this->belongsToMany('\Dcweb\Dcms\Models\Products\Information', 'faq_language_to_products_information', 'faq_language_id', 'products_information_id');
//		}
}
