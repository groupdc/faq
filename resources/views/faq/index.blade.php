@extends("dcms::template/layout")

@section("content")

    <div class="main-header">
        <h1>Faq</h1>
        <ol class="breadcrumb">
            <li><a href="{!! URL::to('admin/dashboard') !!}"><i class="far fa-tachometer-alt-average"></i> Dashboard</a></li>
            <li class="active"><i class="far fa-newspaper"></i> Faq</li>
        </ol>
    </div>

    <div class="main-content">
        <div class="row">
            <div class="col-md-12">
                <div class="main-content-block">

                    @if (Session::has('message'))
                        <div class="alert alert-info">{!! Session::get('message') !!}</div>
                    @endif

                    @can('faq-add')
                        <div class="btnbar btnbar-right"><a class="btn btn-small btn-primary" href="{!! URL::to('/admin/faq/create') !!}">Create new</a></div>
                    @endcan

                    <h2>Overview</h2>
                    
                    <table id="datatable" class="table table-hover table-condensed" style="width:100%">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Online</th>
                            <th>Question</th>
                            <th>Country</th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>

                    <script type="text/javascript">
                        $(document).ready(function () {

                            oTable = $('#datatable').DataTable({
                                "pageLength": 50,
                                "processing": true,
                                "serverSide": true,
                                "ajax": "{{ route('admin.faq.api.table') }}",
                                "columns": [
                                    {data: 'id', name: 'ID'},
                                    {data: 'online', name: 'faq.online'},
                                    {data: 'question', name: 'faq_language.question'},
                                    {data: 'country', name: 'country', searchable: false},
                                    {data: 'edit', name: 'edit', orderable: false, searchable: false}
                                ]
                            });
                        });
                    </script>

                    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.css">

                    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.min.js"></script>
                    <script type="text/javascript" language="javascript"
                            src="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.js"></script>

                </div>
            </div>
        </div>
    </div>
@stop

@section("script")
    <script type="text/javascript">

        languageFilter = "";
        divisionFilter = "";

        $(document).ready(function () {
            $('.sectordd').hide();
            filterLanguage();
            filterDivision();

            $('.changeLanguage').change(function () {
                filterLanguage();
            })

            $('.changeDivision').change(function () {
                filterDivision();
            })

            //Filter toggle
            $('.filter .panel-heading').click(function () {
                $(this).closest('.panel').toggleClass('up');
            });

            $("#btn-reset").click(function () {
                var appendDiv = jQuery($(".condition:last")[0].outerHTML);
                $(".condition").remove();
                $("#filters").prepend(appendDiv.attr('id', '0'));
                $("#0").find('select').val('0');
                $("#0").find('input[type=text]').val('');
            });

            //Add condition
            $('.add-condition').click(function () {
                var id = $(".condition:last").attr('id');
                var appendDiv = jQuery($(".condition:last")[0].outerHTML);

                appendDiv.attr('id', ++id).insertAfter(".condition:last");

                $("#" + id).find('select').val('0');
                $("#" + id).find('input[type=text]').val('');

                return false;
            });

        });
    </script>
@stop
