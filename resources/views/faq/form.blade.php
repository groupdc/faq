@extends("dcms::template/layout")

@section("content")


    <div class="main-header">
        <h1>Faq</h1>
        <ol class="breadcrumb">
            <li><a href="{!! URL::to('admin/dashboard') !!}"><i class="far fa-tachometer-alt-average"></i> Dashboard</a></li>
            <li><a href="{!! URL::to('admin/faq') !!}"><i class="far fa-newspaper"></i> Faq</a></li>
            @if(isset($faq))
                <li class="active"><i class="far fa-pencil"></i> Faq {{$faq->id}}</li>
            @else
                <li class="active"><i class="far fa-plus-circle"></i> Create faq</li>
            @endif
        </ol>
    </div>

    <div class="main-content">
        @if(isset($faq))
            {!! Form::model($faq, array('route' => array('admin.faq.update', $faq->id), 'method' => 'PUT', 'onsubmit'=>'return resetTables();'  )) !!}
        @else
            {!! Form::open(array('url' => 'admin/faq', 'onsubmit'=>'return resetTables();'  )) !!}
        @endif

        <div class="row">
            <div class="col-md-9">
                <div class="main-content-tab tab-container">
                    @if (!is_array($categoryOptionValues) || count($categoryOptionValues)<=0 )    Please first create a <a
                            href="{!! URL::to('admin/faq/categories/create') !!}"> faq category </a>  @else
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="active"><a href="#information" role="tab" data-toggle="tab">F.A.Q.</a></li>
                            {{--<li><a href="#faq" role="tab" data-toggle="tab">Faq</a></li>--}}
                            <li><a href="#products" role="tab" data-toggle="tab">Products</a></li>
                            <li><a href="#plants" role="tab" data-toggle="tab">Plants</a></li>
                            <li><a href="#articles" role="tab" data-toggle="tab">Articles</a></li>
                        </ul>

                        <div class="tab-content">

                            <div id="information" class="tab-pane active">
                                <!-- #information -->
                                @if($errors->any())
                                    <div class="alert alert-danger">{!! Html::ul($errors->all()) !!}</div>
                                @endif

                                @if(isset($languages))
                                    <ul class="nav nav-tabs" role="tablist">
                                        @foreach($languages as $key => $language)
                                            <li class="{!! (( intval(session('overrule_default_by_language_id')) == intval($language->language_id)) ? 'active' : ((intval(session('overrule_default_by_language_id')) == 0 && $key == 0) ? 'active': '') ) !!}"><a href="{!! '#' . $language->language . '-' . $language->country !!}" role="tab"
                                                                                               data-toggle="tab"><img
                                                            src="{!! asset('/packages/Dcms/Core/images/flag-' . strtolower($language->country) . '.svg') !!}" width="16"
                                                            height="16"/> {!! $language->language_name !!}</a></li>
                                        @endforeach
                                    </ul>

                                    <div class="tab-content">
                                        @foreach($languages as $key => $information)
                                            <div id="{!! $information->language . '-' . $information->country !!}" class="tab-pane {!! (( intval(session('overrule_default_by_language_id')) == intval($information->language_id)) ? 'active' : ((intval(session('overrule_default_by_language_id')) == 0 && $key == 0) ? 'active': '')) !!}">
                                                {!! Form::hidden('faq_information_id[' . $information->language_id . ']', $information->id) !!}

                                                <div class="form-group">
                                                    {!! Form::label('sort_id[' . $information->language_id . ']', 'Sort') !!}
                                                    {!! Form::select('sort_id[' . $information->language_id . ']', $sortOptionValues[$information->language_id], (old('sort_id[' . $information->language_id . ']') ? old('sort_id[' . $information->language_id . ']') : $information->sort_id), array('class' => 'form-control')) !!}
                                                </div>

                                                <div class="form-group">
                                                    {!! Form::label('category_id[' . $information->language_id . ']', 'Category') !!}
                                                    {!! isset($categoryOptionValues[$information->language_id])? Form::select('category_id[' . $information->language_id . ']', $categoryOptionValues[$information->language_id], (old('category_id[' . $information->language_id . ']') ? old('category_id[' . $information->language_id . ']') : $information->faq_category_id), array('class' => 'form-control')):'No categories found' !!}
                                                </div>

                                                    <div class="form-group">
                                                        {!! Form::label('question[' . $information->language_id . ']', 'Question') !!}
                                                        {!! Form::text('question[' . $information->language_id . ']', (old('question[' . $information->language_id . ']') ? old('question[' . $information->language_id . ']') : $information->question ), array('class' => 'form-control')) !!}
                                                    </div>

                                                    <div class="form-group">
                                                        {!! Form::label('answer[' . $information->language_id . ']', 'Answer') !!}
                                                        {!! Form::textarea('answer[' . $information->language_id . ']', (old('answer[' . $information->language_id . ']') ? old('answer[' . $information->language_id . ']') : $information->answer ), array('class' => 'form-control ckeditor')) !!}
                                                    </div>



                                            </div>
                                        @endforeach
                                    </div>

                            @endif
                            <!-- #information -->
                            </div>


                            <div id="products" class="tab-pane">
                                <div class="form-group">
                                    <table id="datatable" class="table table-hover table-condensed" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Title</th>
                                                <th>Country</th>
                                                <th>Division</th>
                                                <th>ID</th>         
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>

                            <div id="plants" class="tab-pane">
                                <div class="form-group">
                                    <table id="plants-datatable" class="table table-hover table-condensed" style="width:100%">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Plant</th>
                                            <th>Country</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>

                            <div id="articles" class="tab-pane">
                                <!-- Labels -->
                                <div class="form-group">

                                    <table id="datatable_article" class="table table-hover table-condensed" style="width:100%">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>Title</th>
                                            <th>Country</th>
                                            <th>ID</th> 
                                        </tr>
                                    </thead>
                                    </table>

                                </div>
                            </div>



                        </div>
                </div>
            </div>
            <div class="col-md-3">
                <!-- -->
                <div class="main-content-block">
                    
                    <!-- -->
                    <div class="form-group">
                        {!! Form::checkbox('online', '1', null, array('class' => 'form-checkbox','id'=>'online'))  !!}
                        {!! HTML::decode(Form::label('online', 'Online', array('class' => (isset($faq) && $faq->online==1)?'checkbox active':'checkbox'))) !!}
                    </div>
                    
                </div>
                <!-- -->
                <!-- -->
            </div>
            @endif
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="main-content-block">
                    {!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
                    <a href="{!! URL::previous() !!}" class="btn btn-default">Cancel</a>
                </div>
            </div>
        </div>
        
        <div id="removed-faq"></div>
        {!! Form::close() !!}
    </div>

@stop

@section("script")

    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/js/bootstrap.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/js/bootstrap-datetimepicker.min.js') !!}"></script>
    <link rel="stylesheet" type="text/css" href="{!! asset('/packages/Dcms/Core/css/bootstrap-datetimepicker.min.css') !!}">

    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckeditor/ckeditor.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckeditor/adapters/jquery.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckfinder/ckfinder.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckfinder/ckbrowser.js') !!}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            //CKFinder for CKEditor
            CKFinder.setupCKEditor(null, '/packages/Dcms/Core/ckfinder/');

            //CKFinder
            $(".browse-server").click(function () {
                BrowseServer('Images:/faq/', 'thumbnail');
            })

            $("body").on("click", ".browse-server-files", function () {
                var returnid = $(this).attr("id").replace("browse_", "");
                BrowseServer('Files:/', returnid);
            });

            //CKEditor
            $("textarea[id='description']").ckeditor();
            $("textarea[id='body']").ckeditor();

            //Bootstrap Tabs
            $(".tab-container .nav-tabs a").click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            })

            //Datepicker
            //ToggleDate();

            $(".setdate label").click(function () {
                //ToggleDate();
            });

            //pagetree
            $(".country span").click(function () {
                $(this).find('i').toggleClass('fa-minus-square');
                $(this).next().toggleClass('active');
            });

            //Advanced
            $("#advanced").click(function() {
                $('.advanced').toggleClass('d-none');
                return false;
            });

            $(".add-faq-row").click(function () {
                theid = $(this).attr("id").substring(12,999);
                $.ajax({
                    method: 'POST',
                    url: "{{ route('admin.faq.api.faqrow') }}",
                    data: {_token: '{{ csrf_token() }}',language_id:theid.substring((theid.length)-1) },
                    success: function (result) {
                        $("#"+theid+" tbody").append(result);
                        $('.tab-pane.active tbody tr:last td textarea').ckeditor();
                    }
                });
            });

        $(document).on('click', '.remove-faq-row', function () {
            var id = $(this).data('id');
            $(this).closest('tr').remove();
            $('#removed-faq').append('<input type="hidden" name="removed_faq[]" value="' + id + '">');
        });

        });

      
    </script>

    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.css">
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.js"></script>

    <script type="text/javascript">
        productsTable = null;
        plantsTable = null;
        function resetTables(){
            productsTable.search('').draw();
            plantsTable.search('').draw();
            return true;
        }

        <?php $id_helper = (isset($faq)?$faq->id:0); ?>
        $(document).ready(function() {
            productsTable = $('#datatable').DataTable({
                "processing": true,
                "paging": false,
                "ajax": "{{ route('admin.faq.api.products.table',$id_helper) }}",
                "columns": [
                    {data: 'radio', name: 'radio'},
                    {data: 'title', name: 'title'},
                    {data: 'language', name: 'language'},
                    {data: 'catalogue', name: 'catalogue'},
                    {data: 'information_group_id', name: 'information_group_id'},
                ],
                "createdRow": function( row, data, cells ) {
                    if (~data['radio'].indexOf('checked')) {   
                    $(row).addClass('selected');
                    }
                }
            });

            plantsTable = $('#plants-datatable').DataTable({
                "paging": false,
                "processing": true,
                "serverSide": false,
                "ajax": "{{ route('admin.faq.api.plants.table', $id_helper) }}",
                "columns": [
                    {data: 'radio', name: 'radio', width: '5%'},
                    {data: 'common', name: 'common'},
                    {data: 'language', name: 'language'}
                ]
            });
            
            $('#datatable').on('click', 'tbody tr', function (e) {
                if($(e.target).is('input[type=checkbox]')) {
                    $(this).toggleClass('selected');
                } else {
                    var clicked = $(this).find('input:checkbox');
                    if ( clicked.is(':checked')) {
                        clicked.prop('checked', false);
                    } else {
                        clicked.prop('checked', true);
                    }
        
                    $(this).toggleClass('selected');
                }
            });
            
            $('#plants-datatable').on('click', 'tbody tr', function (e) {
                if($(e.target).is('input[type=checkbox]')) {
                    $(this).toggleClass('selected');
                } else {
                    var clicked = $(this).find('input:checkbox');
                    if ( clicked.is(':checked')) {
                        clicked.prop('checked', false);
                    } else {
                        clicked.prop('checked', true);
                    }
        
                    $(this).toggleClass('selected');
                }
            });
        
        articlesTable = $('#datatable_article').DataTable({
                      "paging": false,
                      "processing": true,
                      "serverSide": false,
                      
                    "ajax": "{{ route('admin.faq.api.articles.table', $id_helper) }}",
                      "columns": [
                          {data: 'radio', name: 'radio'},
                          {data: 'title', name: 'title'},
                          {data: 'language', name: 'language'},
                          {data: 'article_id', name: 'article_id'},
                      ],
                      "createdRow": function( row, data, cells ) {
                            if (~data['radio'].indexOf('checked')) { 
                            $(row).addClass('selected');
                            }
                        }
                  });

        $('#datatable_article').on('click', 'tbody tr', function () {
            if($(e.target).is('input[type=checkbox]')) {
            $(this).toggleClass('selected');
            } else {
                var clicked = $(this).find('input:checkbox');
                if ( clicked.is(':checked')) {
                    clicked.prop('checked', false);
                } else {
                    clicked.prop('checked', true);
                }
    
                $(this).toggleClass('selected');
            }
        });      


        });
    </script>
@stop

@section('style')
<style type="text/css">
.d-none {
    display: none;
}
</style>
@stop