<?php

Route::group(['middleware' => ['web']], function () {
    Route::group(array("prefix" => "admin", "as"=>"admin."), function () {
        Route::group(['middleware' => 'auth:dcms'], function () {

                //faq
            Route::group(array("prefix" => "faq" ,"as"=>"faq."), function () {
                Route::get('{id}/copy', array('as'=>'copy', 'uses' => 'FaqController@copy'));
                
                //CATEGORIES
                Route::group(array("prefix" => "categories", "as"=>"categories."), function () {
                    Route::get('{id}/copy', array('as'=>'{id}.copy', 'uses' => 'CategoryController@copy'));
                    Route::any('api/table', array('as'=>'api.table', 'uses' => 'CategoryController@getDatatable'));
                });
                Route::resource('categories', '\Dcms\Faq\Http\Controllers\CategoryController');

                //API
                Route::group(array("prefix" => "api", "as"=>"api."), function () {
                    Route::any('table', array('as'=>'table', 'uses' => 'FaqController@getDatatable'));
                    Route::any('products/table/{faq_id?}', array('as'=>'products.table', 'uses' => 'FaqController@getProductsDatatable'));
                    Route::any("relation/table/{product_id?}", array("as"=>"relation.table", "uses" => "FaqController@getRelationDatatable"));
                    Route::any("plants/table/{faq_id?}", array("as"=>"plants.table", "uses" => "FaqController@getPlantsDatatable"));
                    Route::any("articles/table/{faq_id?}", array("as"=>"articles.table", "uses" => "FaqController@getArticlesDatatable"));
                    Route::any("faqrow", array("as"=>"faqrow", "uses" => "FaqController@getFaqrow"));
                });
            });
            Route::resource('faq', 'FaqController');
        });
    });
});
