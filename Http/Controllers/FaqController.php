<?php

namespace Dcms\Faq\Http\Controllers;

use DB;
use Auth;
use Form;
use Mail;
use View;
use Input;
use Session;
use DateTime;
use Redirect;
use Validator;
use DataTables;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Dcms\Plants\Models\Plant;
use Dcms\Faq\Models\Faq;
use Dcms\Products\Models\Product;
use App\Http\Controllers\Controller;
use Dcms\Faq\Models\Detail;
use Dcms\Faq\Models\Category;
use Illuminate\Support\Facades\Schema;

class FaqController extends Controller
{
    public $faq_language_Columnnames = [];
    public $faq_Columnnames = [];
    public $faq_ColumnNamesDefaults = [];

    public $faqlanguageFormTemplate = "";
    public $faqFormTemplate = "";

    public $extendgeneralTemplate = "";

    public function __construct()
    {
        $this->middleware('permission:faq-browse')->only('index');
        $this->middleware('permission:faq-add')->only(['create', 'store']);
        $this->middleware('permission:faq-edit')->only(['edit', 'update']);
        $this->middleware('permission:faq-delete')->only('destroy');

        //note startdate and enddate are defaults, since these are always there, and have a specific value (not simple text, or bool)
        $this->faq_Columnnames = [
            'online'        => 'online',
        ];

        $this->faq_language_Columnnames = [
            'faq_category_id' => 'category_id',
            'question'               => 'question',
            'answer'         => 'answer',
            'sort_id' => 'sort_id',
            'slug' => 'slug',
        ];

        $this->faq_ColumnNamesDefaults = [
            ];

        $this->faqlanguageFormTemplate = null;
        $this->faqFormTemplate = null;
    }

    public static function getFaqDetailByLanguage($language_id = null)
    {
        return Detail::where("language_id", "=", $language_id)->lists("title", "id");
    }

    public static function getDropdownFaqByLanguage($language_id = null)
    {
        $dropdownvalues = FaqController::getFaqDetailByLanguage($language_id);

        return Form::select("faq[" . $language_id . "][]", $dropdownvalues, [3, 4, 5, 6], ["id" => "faq-" . $language_id, "multiple" => "multiple"]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $sectors = DB::connection('project')
            ->table('vwpages_language')
            ->select(
                'title',
                'language_id',
                'division'
            )
            ->where('depth', '=', '2')
            ->orderBy('title')
            ->get();

        // load the view
        return View::make('dcmsfaq::faq/index')->with('sectors', $sectors);
    }

    public function getDatatable()
    {
        $query = DB::connection('project')
            ->table('faq')
            ->select(
                'faq.id',
                'faq.online',
                'faq_language.question',
                'faq_language.id as faqlangid',
                (DB::connection("project")->raw('Concat("<img src=\'/packages/Dcms/Core/images/flag-",lcase(country),".svg\' style=\'width:16px; height:auto;\'>") as country'))
            )
            ->join('faq_language', 'faq.id', '=', 'faq_language.faq_id')
            ->leftJoin('languages', 'faq_language.language_id', '=', 'languages.id');
            
        if (intval(session('overrule_default_by_language_id')) > 0) {
            $query->where('faq_language.language_id', session('overrule_default_by_language_id'));
        }

        return Datatables::queryBuilder($query)
                        ->addColumn('edit', function ($model) {
                            return '<form method="POST" action="/admin/faq/'.$model->faqlangid.'" accept-charset="UTF-8" class="pull-right">
							<input name="_token" type="hidden" value="'.csrf_token().'">
							<input name="_method" type="hidden" value="DELETE">
							<a class="btn btn-xs btn-default" href="/admin/faq/'.$model->id.'/edit"><i class="far fa-pencil"></i></a>
							<button class="btn btn-xs btn-default" type="submit" value="Delete this faq" onclick="if(!confirm(\'Are you sure to delete this item?\')){return false;};"><i class="far fa-trash-alt"></i></button>
						</form>';
                        })
                        ->rawColumns(['country','edit'])
                        ->make(true) ;
    }

    /**
     * get the data for DataTable JS plugin.
     *
     * @return Response
     */
    public function getProductsDatatable($faq_id = 0)
    {
        $queryA = DB::connection('project')
        ->table('products_information as x')
        ->select(
            (
                DB::connection("project")->raw('
                case when (select count(*) from faq_to_products_information_group where faq_to_products_information_group.information_group_id = x.information_group_id and faq_id = "'.$faq_id.'") > 0 then 1 else 0 end as checked,
                catalogue,
                title,
                languages.country,
                information_group_id')
            )
        )
        ->leftJoin('languages', 'x.language_id', '=', 'languages.id')
        ->whereRaw('x.id IN (SELECT product_information_id FROM products_to_products_information WHERE product_id IN ( SELECT id FROM products WHERE online = 1 ) )')
        ->whereNotNull('x.information_group_id')
        ->orderBy('checked', 'DESC');

        $queryB = clone $queryA;
        $queryB->whereRaw('case when (select count(*) from faq_to_products_information_group where faq_to_products_information_group.information_group_id = x.information_group_id and faq_id = "'.$faq_id.'") > 0 then 1 else 0 end = 1');
        
        if (intval(session('overrule_default_by_language_id')) > 0) {
            $queryA->where('x.language_id', session('overrule_default_by_language_id'));
        }

        return Datatables::queryBuilder($queryB->union($queryA)->orderBy('checked', 'desc'))
                ->addColumn('radio', function ($model) {
                    return '<input type="checkbox" name="information_group_id[]" value="'.$model->information_group_id.'" '.($model->checked == 1?'checked="checked"':'').' id="chkbox_'.$model->information_group_id.'" > ';
                })
                ->addColumn('language', function ($model) {
                    return '<img src="/packages/Dcms/Core/images/flag-'.strtolower($model->country).'.svg" style="width:16px; height: auto;" alt="">';
                })
                ->rawColumns(['radio','language'])
                ->make(true);
    }

    public function getRelationDatatable($product_id = 0)
    {
        $information_group_id = 0;
        $ProductInformation = Product::with('information')->find($product_id);

        if (isset($ProductInformation->information) && !is_null($ProductInformation->information) && $ProductInformation->information()->count() > 0) {
            foreach ($ProductInformation->information as $I) {
                if (!is_null($I->information_group_id)) {
                    $information_group_id = $I->information_group_id;
                    break;
                }
            }
        }

        $queryA = DB::connection('project')
                                        ->table('faq_language as x')
                                        ->select(
                                            (
                                                DB::connection("project")->raw('
																	faq_id,
                                                                    title,
                                                                    languages.country,
																	case when (select count(*) from faq_to_products_information_group where faq_to_products_information_group.faq_id = x.faq_id and information_group_id = "'.$information_group_id.'") > 0 then 1 else 0 end as checked
																')
                                            )
                                        )
                                        ->leftJoin('languages', 'x.language_id', '=', 'languages.id')
                                        ->orderBy('checked', 'DESC');

        $queryB = clone $queryA;
        $queryB->whereRaw('case when (select count(*) from faq_to_products_information_group where faq_to_products_information_group.faq_id = x.faq_id and information_group_id = "'.$information_group_id.'") > 0 then 1 else 0 end = 1');
                                           
        if (intval(session('overrule_default_by_language_id')) > 0) {
            $queryA->where('x.language_id', session('overrule_default_by_language_id'));
        }

        return Datatables::queryBuilder($queryB->union($queryA)->orderBy('checked', 'desc'))
                        ->addColumn('radio', function ($model) {
                            return '<input type="checkbox" name="faq_id[]" value="'.$model->faq_id.'" '.($model->checked == 1?'checked="checked"':'').' id="chkbox_'.$model->faq_id.'" > ';
                        })
                        ->addColumn('language', function ($model) {
                            return '<img src="/packages/Dcms/Core/images/flag-'.strtolower($model->country).'.svg" style="width:16px; height: auto;" alt="">';
                        })
                        ->rawColumns(['radio','language'])
                        ->make(true) ;
    }

    public function getPlantsDatatable($faq_id = null)
    {
        $queryA = DB::connection('project')
        ->table('plants_language as x')
        ->select(
            (
                DB::connection("project")->raw('
                case when (select count(*) from faq_to_plant where faq_to_plant.plant_id = x.plant_id and faq_id = "'.$faq_id.'") > 0 then 1 else 0 end as checked,
                languages.country,
                x.plant_id, 
                x.common')
            )
        )
        ->leftJoin('languages', 'x.language_id', '=', 'languages.id')
        ->orderBy('checked', 'DESC');
        
        $queryB = clone $queryA;
        $queryB->whereRaw('case when (select count(*) from faq_to_plant where faq_to_plant.plant_id = x.plant_id and faq_id = "'.$faq_id.'") > 0 then 1 else 0 end = 1');
        
        if (intval(session('overrule_default_by_language_id')) > 0) {
            $queryA->where('x.language_id', session('overrule_default_by_language_id'));
        }
        
        return Datatables::queryBuilder($queryB->union($queryA)->orderBy('checked', 'desc'))
                ->addColumn('radio', function ($model) {
                    return '<input type="checkbox" name="plant_ids[]" value="'.$model->plant_id.'" '.($model->checked == 1?'checked="checked"':'').' id="chkbox_'.$model->plant_id.'" > ';
                })
                ->addColumn('language', function ($model) {
                    return '<img src="/packages/Dcms/Core/images/flag-'.strtolower($model->country).'.svg" style="width:16px; height: auto;" alt="">';
                })
                ->rawColumns(['radio','language'])
                ->make(true);
    }
    
    public function getArticlesDatatable($faq_id = null)
    {
        $queryA = DB::connection('project')
        ->table('articles_language as x')
        ->select(
            (
                DB::connection("project")->raw('
                case when (select count(*) from article_to_faq where article_to_faq.article_id = x.article_id and faq_id = "'.$faq_id.'") > 0 then 1 else 0 end as checked,
                languages.country,
                x.article_id, 
                x.title')
            )
        )
        ->leftJoin('languages', 'x.language_id', '=', 'languages.id')
        ->orderBy('checked', 'DESC');
        
        $queryB = clone $queryA;
        $queryB->whereRaw('case when (select count(*) from article_to_faq where article_to_faq.article_id = x.article_id and faq_id = "'.$faq_id.'") > 0 then 1 else 0 end = 1');
        
        if (intval(session('overrule_default_by_language_id')) > 0) {
            $queryA->where('x.language_id', session('overrule_default_by_language_id'));
        }
        
        return Datatables::queryBuilder($queryB->union($queryA)->orderBy('checked', 'desc'))
                ->addColumn('radio', function ($model) {
                    return '<input type="checkbox" name="article_ids[]" value="'.$model->article_id.'" '.($model->checked == 1?'checked="checked"':'').' id="chkbox_'.$model->article_id.'" > ';
                })
                ->addColumn('language', function ($model) {
                    return '<img src="/packages/Dcms/Core/images/flag-'.strtolower($model->country).'.svg" style="width:16px; height: auto;" alt="">';
                })
                ->rawColumns(['radio','language'])
                ->make(true);
    }

    public function getInformation($id = null)
    {
        if (is_null($id)) {
            return DB::connection("project")->table("languages")->select((DB::connection("project")->raw("'' as question, '' as faq_category_id, '' as answer,  (select max(sort_id) from faq_language as X  where X.language_id = languages.id) as maxsort, (select max(sort_id)+1 from faq_language as X  where X.language_id = languages.id) as sort_id, '' as id")), "id as language_id", "language", "country", "language_name")->get();
        } else {
            return DB::connection("project")->select('
													SELECT language_id, languages.language, languages.country, languages.language_name, faq_category_id, faq_language.id, faq_id, question, answer, sort_id,  (select max(sort_id) from faq_language as X  where X.language_id = faq_language.language_id) as maxsort
													FROM faq_language
													LEFT JOIN languages on languages.id = faq_language.language_id
													LEFT JOIN faq on faq.id = faq_language.faq_id
													WHERE  languages.id is not null AND  faq_id = ?
													UNION
													SELECT languages.id , language, country, language_name, \'\' , \'\' ,  \'\' , \'\' , \'\' , NULL as sort_id, (select max(sort_id) from faq_language where language_id = languages.id) as maxsort
													FROM languages
													WHERE id NOT IN (SELECT language_id FROM faq_language WHERE faq_id = ?) ORDER BY 1
													', [$id, $id]);
        }
    }

    public function getExtendedModel()
    {
        //do nothing let the extend class hook into this
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $languages = $this->getInformation();

        // load the create form (app/views/faq/create.blade.php)
        return View::make('dcmsfaq::faq/form')
        ->with('languages', $languages)
            ->with('categoryOptionValues', Category::OptionValueTreeArray(false))
            ->with('sortOptionValues', $this->getSortOptions($languages, 1))
            ->with('faqlanguageFormTemplate', $this->faqlanguageFormTemplate)
            ->with('faqFormTemplate', $this->faqFormTemplate)
            ->with('extendgeneralTemplate', ['template' => $this->extendgeneralTemplate, 'model' => $this->getExtendedModel()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $faq = Faq::find($id);
        $cat = $faq->category;

        // show the view and pass the nerd to it
        return View::make('dcms::faq/show')
            ->with('faq', $faq)
            ->with("category", $cat->title);
    }

    public function getSortOptions($model, $setExtra = 0)
    {
        foreach ($model as $M) {
            $increment = 0;
            if ($setExtra > 0) {
                $increment = $setExtra;
            }
            if (intval($M->id) <= 0 && !is_null($M->maxsort)) {
                $increment = 1;
            }

            $maxSortID = $M->maxsort;
            if (is_null($maxSortID)) {
                $maxSortID = 1;
            }

            for ($i = 1; $i <= ($maxSortID + $increment); $i++) {
                $SortOptions[$M->language_id][$i] = $i;
            }
        }

        return $SortOptions;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $faq = Faq::find($id);
       
        $objlanguages = $this->getInformation($id);

        return View::make('dcmsfaq::faq/form')
            ->with('languages', $objlanguages)
            ->with('faq', $faq)
            ->with('categoryOptionValues', Category::OptionValueTreeArray(false))
            ->with('sortOptionValues', $this->getSortOptions($objlanguages))
            ->with('faqlanguageFormTemplate', $this->faqlanguageFormTemplate)
            ->with('faqFormTemplate', $this->faqFormTemplate)
            ->with('extendgeneralTemplate', ['template' => $this->extendgeneralTemplate, 'model' => $this->getExtendedModel()]);
    }

    private function validateFaqForm()
    {
        $rules = [
            //	'title' => 'required'
        ];
        $validator = Validator::make(request()->all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()//to('admin/products/' . $id . '/edit')
            ->withErrors($validator)
                ->withInput();
        } else {
            return true;
        }
    }
    
    public function saveFaq(Request $request, $faq_id = null)
    {
        if (request()->has('removed_faq')) {
            Faq::destroy(request()->get('removed_faq'));
        }

        if (request()->has('faq')) {
            foreach (request()->get('faq') as $faq_id => $data) {
                $faq_find = Faq::find($faq_id);
                $faq = $faq_find ?: new Faq();
                $faq->faq_id = $faq_id;
                $faq->language_id = $data['language_id'];
                $faq->question = $data['question'];
                $faq->answer = $data['answer'];
                $faq->save();
            }
        }
    }


    private function saveFaqProperties(Request $request, $faqid = null)
    {
        $notifymoderator = false;
        // do check if the given id is existing.
        if (!is_null($faqid) && intval($faqid) > 0) {
            $Faq = Faq::find($faqid);
        }

        if (!isset($Faq) || is_null($Faq)) {
            $Faq = new Faq;
        }

        foreach ($request->get("question") as $language_id => $title) {
            if (strlen(trim($request->get("question")[$language_id])) > 0 && strlen(trim($request->get("answer")[$language_id])) > 0) {
                //----------------------------------------
                // once there is a title set
                // we can set the properties to the faq
                // and instantly return the faq model
                // by default nothing is return so the
                // script may stop.
                //----------------------------------------
                
                foreach ($this->faq_Columnnames as $column => $inputname) {
                    if ($request->has($inputname)) {
                        $Faq->$column = $request->get($inputname);
                    } elseif (array_key_exists($inputname, $this->faq_ColumnNamesDefaults)) {
                        $Faq->$column = $this->faq_ColumnNamesDefaults[$inputname];
                    } else {
                        $Faq->$column = null;
                    }
                }

                $Faq->save();

                return $Faq;
                break; // we only have to save the global settings once.
            }
        }
        //does not return anything by defuault... (may be false dunno - find out)
    }

    private function saveFaqDetail(Request $request, Faq $Faq, $givenlanguage_id = null)
    {
        $input = $request->all();
        $Detail = null;
        $plantPageSlugs = [];
         
        foreach ($input["question"] as $language_id => $question) {
            if (strlen(trim($input["question"][$language_id])) > 0 && strlen(trim($input["answer"][$language_id])) > 0) {
                if ((is_null($givenlanguage_id) || ($language_id == $givenlanguage_id))) {
                    $Detail = null;
                    $newInformation = true;
                    $Detail = Detail::find($input["faq_information_id"][$language_id]);

                    if (is_null($Detail) === true) {
                        $Detail = new Detail();
                    } else {
                        $newInformation = false;
                    }

                    $oldSortID = null;
                    if ($newInformation == false && !is_null($Detail->sort_id) && intval($Detail->sort_id) > 0) {
                        $oldSortID = intval($Detail->sort_id);
                    }

                    $Detail->language_id = $language_id;
                    foreach ($this->faq_language_Columnnames as $column => $inputname) {
                        if (!in_array($column, ["slug", "path", "faq_category_id"])) {
                            $Detail->$column = $input[$inputname][$language_id];
                        }
                    }

                    if (array_key_exists($language_id, $input[$this->faq_language_Columnnames["faq_category_id"]])) {
                        $Detail->faq_category_id = ($input[$this->faq_language_Columnnames["faq_category_id"]][$language_id] == 0 ? null : $input[$this->faq_language_Columnnames["faq_category_id"]][$language_id]);
                    }

                    $url_slug = Str::slug($input[$this->faq_language_Columnnames["question"]][$language_id]);

                    //get all existing slugs
                    $existingSlugs = Detail::where('id', '<>', $Detail->id)->where('language_id', $language_id)->get('slug')->pluck('slug')->toArray();
                    
                    $notAllowedSlugs = array_merge($existingSlugs, $plantPageSlugs);

                    if (in_array($url_slug, $notAllowedSlugs)) {
                        for ($appendix = 1;$appendix<=15;$appendix++) {
                            if (!in_array($url_slug.'-'.$appendix, $notAllowedSlugs)) {
                                $url_slug= $url_slug.'-'.$appendix;
                                break;
                            }
                        }
                    }
                    $Detail->slug = $url_slug;

                    $Detail->save();
                    $Faq->detail()->save($Detail);

                    $sort_incrementstatus = "0"; //the default
                    if (is_null($oldSortID) || $oldSortID == 0) {
                        //update all where sortid >= input::sortid
                        $updateFaqDetailSorts = Detail::where('language_id', '=', $language_id)->where('sort_id', '>=', $input['sort_id'][$language_id])->where('id', '<>', $Detail->id)->get(['id', 'sort_id']);
                        $sort_incrementstatus = "+1";
                    } elseif ($oldSortID > $input['sort_id'][$language_id]) {
                        $updateFaqDetailSorts = Detail::where('language_id', '=', $language_id)->where('sort_id', '>=', $input['sort_id'][$language_id])->where('sort_id', '<', $oldSortID)->where('id', '<>', $Detail->id)->get(['id', 'sort_id']);
                        $sort_incrementstatus = "+1";
                    } elseif ($oldSortID < $input['sort_id'][$language_id]) {
                        $updateFaqDetailSorts = Detail::where('language_id', '=', $language_id)->where('sort_id', '>', $oldSortID)->where('sort_id', '<=', $input['sort_id'][$language_id])->where('id', '<>', $Detail->id)->get(['id', 'sort_id']);
                        $sort_incrementstatus = "-1";
                    }

                    if ($sort_incrementstatus <> "0") {
                        if (isset($updateFaqDetailSorts) && count($updateFaqDetailSorts) > 0) {
                            foreach ($updateFaqDetailSorts as $updateFaqDetail) {
                                if ($sort_incrementstatus == "+1") {
                                    $updateFaqDetail->sort_id = intval($updateFaqDetail->sort_id) + 1;
                                    $updateFaqDetail->save();
                                } elseif ($sort_incrementstatus == "-1") {
                                    $updateFaqDetail->sort_id = intval($updateFaqDetail->sort_id) - 1;
                                    $updateFaqDetail->save();
                                }
                            }//end foreach($updateFaqDetailSorts as $Information)
                        }//end 	if (count($updateFaqDetailSorts)>0)
                    }//$sort_incrementstatus <> "0"
                }
            }
        }

        return $Detail;
    }

    public function getSelectedProductsInformation($faq_id = null)
    {
        return DB::connection("project")->select('	SELECT products_information_id, faq_language_id
													FROM faq_language_to_products_information
													WHERE faq_language_id IN (SELECT id FROM faq_language WHERE faq_id = ?)', [$faq_id]);
    }

    public function setProductsInformationOptionValues($objselected_pages)
    {
        $ProductsOptionValuesSelected = [];
        if (count($objselected_pages) > 0) {
            foreach ($objselected_pages as $obj) {
                $ProductsOptionValuesSelected[$obj->products_information_id] = $obj->products_information_id;
            }
        }

        return $ProductsOptionValuesSelected;
    }

    public function saveFaqToProductInformation(Request $request, Faq $Faq)
    {
        $Faq->productinformation()->detach();
        if ($request->has("information_group_id") && count($request->get("information_group_id")) > 0) {
            foreach ($request->get("information_group_id") as $i => $information_group_id) {
                $Faq->productinformation()->attach($information_group_id, ['information_group_id' => $information_group_id]);
            }
        }
    }

    public function saveFaqToPlants(Request $request, Faq $Faq)
    {
        $plant_ids = $request->has('plant_ids') ? $request->get('plant_ids') : [];
        $Faq->plants()->sync($plant_ids);
    }

    public function saveFaqToArticle(Request $request, Faq $Faq)
    {
        $article_ids = $request->has('article_ids') ? $request->get('article_ids') : [];
        $Faq->articles()->sync($article_ids);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        if ($this->validateFaqForm() === true) {
            $Faq = $this->saveFaqProperties($request);

            if (!is_null($Faq)) {
                $this->saveFaqDetail($request, $Faq);
                $this->saveFaqToProductInformation($request, $Faq);
                $this->saveFaqToPlants($request, $Faq);
                $this->saveFaqToArticle($request, $Faq);
                $this->setCanonicalUrl($request, $Faq);
                $this->saveFaq($request, $Faq->id);
            }

            // redirect
            Session::flash('message', 'Successfully created faq!');
        
            return Redirect::to('admin/faq');
        } else {
            return $this->validateFaqForm();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        if ($this->validateFaqForm() === true) {
            $Faq = $this->saveFaqProperties($request, $id);

            if (!is_null($Faq)) {
                $this->saveFaqDetail($request, $Faq);
                $this->saveFaqToProductInformation($request, $Faq);
                $this->saveFaqToPlants($request, $Faq);
                $this->saveFaqToArticle($request, $Faq);
                $this->setCanonicalUrl($request, $Faq);
                $this->saveFaq($request, $Faq->id);
            }

            // redirect
            Session::flash('message', 'Successfully updated faq!');

            return Redirect::to('admin/faq');
        } else {
            return $this->validateFaqForm();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $Detail = Detail::find($id);

        $mainFaqID = $Detail->faq_id;

        //find the sortid of this product... so all the above can descent by 1
        $updateInformations = Detail::where('language_id', '=', $Detail->language_id)->get(['id', 'sort_id']);
        if (isset($updateInformations) && count($updateInformations) > 0) {
            foreach ($updateInformations as $uInformation) {
                $uInformation->sort_id = intval($uInformation->sort_id) - 1;
                $uInformation->save();
            }//end foreach($updateInformations as $Information)
        }//end 	if (count($updateInformations)>0)

        $Detail->delete();

        if (Detail::where("faq_id", "=", $mainFaqID)->count() <= 0) {
            Faq::destroy($mainFaqID);
        }

        Session::flash('message', 'Successfully deleted the faq!');

        return Redirect::to('admin/faq');
    }

    public function verifySlug(Faq $Faq)
    {
        return "ok";
    }
    
    public function setUrlSlugNew(Request $request)
    {
        return "ok";
    }

    public function setCanonicalUrl(Request $request, $faq)
    {
    }
}
